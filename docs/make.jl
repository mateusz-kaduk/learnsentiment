using Documenter, LearnSentiment

makedocs(
    modules = [LearnSentiment],
    checkdocs = :exports,
    sitename = "Learn sentiment",
    pages = Any["index.md"],
    repo = "https://gitlab.com/mateusz-kaduk/learnsentiment/blob/{commit}{path}#{line}"
)
