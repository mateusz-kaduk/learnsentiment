# Documentation

## Pre-processing

```@docs
max_pad(corpus)
```

```@docs
pad_corpus(corpus)
```

## Training

```@docs
create_model()
```

```@docs
train_model(m, learning_rate = 0.01, epochs=4000)
```
