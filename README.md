# Learn sentiment with Flux

[![pipeline status](https://gitlab.com/mateusz-kaduk/learnsentiment/badges/master/pipeline.svg)](https://gitlab.com/mateusz-kaduk/learnsentiment/-/commits/master)
[![coverage report](https://gitlab.com/mateusz-kaduk/learnsentiment/badges/master/coverage.svg)](https://gitlab.com/mateusz-kaduk/learnsentiment/-/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://mateusz-kaduk.gitlab.io/learnsentiment/)
