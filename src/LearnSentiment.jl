module LearnSentiment
using Flux

export max_pad
export pad_corpus
export create_model
export train_model

"""
    max_pad(corpus)

A function that returns maximum number of words from all sentences in a text corups.
"""
function max_pad(corpus)
        return maximum(length.(split.(corpus)))
end

"""
    pad_corpus(corpus)

A function that pads whole corpus to the longest sentence.
"""
function pad_corpus(corpus)
    pad_size = max_pad(corpus)
    M = []
    for doc_idx in 1:length(corpus)
        words = split(corpus[doc_idx])
        if length(words) >= pad_size
            tk_ind = [tk(w) for w in words[1:pad_size]]
        else
            tk_ind = zeros(Int, pad_size-length(words))
            tk_ind = vcat([tk(w) for w in words], tk_ind)
        end
        doc_idx == 1 ? M = tk_ind' : M=vcat(M, tk_ind')
    end
    return M
end

corpus = ["well done",
        "good work",
        "great effort",
        "nice work",
        "excellent",
        "weak",
        "poor effort",
        "not good",
        "poor work",
        "could have done better"]

# positve or negative sentiment to each 'document' string
y = [true true true true true false false false false false]

word_dict = Dict([(w,i) for (i,w) in enumerate(Set(Iterators.flatten(split.(corpus))))])
tk = (s) -> get(word_dict, s, 0)

padded_corpus = pad_corpus(corpus)  # Padding
x = padded_corpus' # Columns documents, rows words

# Combine data with sentiment labels
data = [(x,y)]

N = size(padded_corpus, 1)  #Number of documents (10)
pad_size = size(padded_corpus, 2)  # Max padding (4)
max_features = 8
vocab_size = 20

# Define embedding layer
struct EmbeddingLayer
    W
    EmbeddingLayer(mf, vs) = new(Flux.glorot_normal(mf, vs))
end
@Flux.functor EmbeddingLayer # Needs autograd

(m::EmbeddingLayer)(x) = m.W * Flux.onehotbatch(reshape(x, pad_size*N), 0:vocab_size-1)

"""
    create_model()

A function that creates a simple shallow network model.
"""
function create_model()
    # Define model
    m = Chain(EmbeddingLayer(max_features, vocab_size),
    x -> reshape(x, max_features, pad_size, N),
    x -> sum(x, dims=2),
    x -> reshape(x, max_features, N),
    Dense(max_features, 1, σ)
    )
    return m
end

"""
    train_model(m, learning_rate = 0.01, epochs=4000)

A function that trains a model.
"""
function train_model(m, learning_rate = 0.01, epochs=4000)
    # To calculate accuracy
    loss_h=[]
    accuracy_train=[]
    accuracy(x, y) = sum(x .== y)/length(x)

    # Loss and optimizer
    loss(x, y) = sum(Flux.Losses.binarycrossentropy.(m(x), y))
    optimizer = Flux.Descent(learning_rate)

    # Train model
    for epoch in 1:epochs
        Flux.train!(loss, Flux.params(m), data, optimizer)
            println(loss(x, y), " ", accuracy(m(x).>0.5,y))
            push!(loss_h, loss(x, y))
            push!(accuracy_train, accuracy(m(x).>0.5,y))
    end
    println(m(x).>0.5)
    accuracy(m(x).>0.5,y)
end

end # module
